Assignment-1 :

1.Install node.js version v16.14.0 on ubuntu server 22.04 by using this block
https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-ubuntu-22-04.

2.Install Docker version 23.0.6 on ubuntu server 22.04 by using this block
https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-22-04.

3.Create Dockerfile
command: sudo vi Dockerfile
[](/home/deeksha/Pictures/Screenshots/Screenshot from 2023-05-11 15-11-12.png)


                                                                                                                            4. Build Dockerfile
sudo docker build -f Dockerfile -t react-app .

5.Run Dockerfile
sudo docker run -it --name reactapp -p 3000:3000 react-app
                                                                                                                            6. Screenshot of Output:
[](/home/deeksha/Pictures/Screenshots/Screenshot from 2023-05-11 15-08-35.png)
